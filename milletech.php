<?php
require "db.php";

$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => "https://www.milletech.se/invoicing/export/customers",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "postman-token: 41cf9c1f-68fe-07e0-895d-38ea8921965c"
    ),
));

$response = curl_exec($curl);

$response = json_decode($response, true);
$err = curl_error($curl);

curl_close($curl);


foreach ($response as $customer) {

    if (isset($customer['id'])) {
        $sql_customers = "INSERT INTO `users`(
        `id`, `firstname`, `lastname`, `gender`, `customer_activated`, `group_id`, `customer_company`, `default_billing`, `default_shipping`,
        `is_active`, `created_at`, `updated_at`, `customer_invoice_email`, `customer_extra_text`, `customer_due_date_period`
      )
      VALUES (
        :id, :firstname, :lastname, :gender, :customer_activated, :group_id, :customer_company, :default_billing, :default_shipping, 
        :is_active, :created_at, :updated_at, :customer_invoice_email, :customer_extra_text, :customer_due_date_period
      )";
        $stm_insert_customers = $pdo->prepare($sql_customers);
        $stm_insert_customers->execute([
            'id' => $customer['id'],
            'firstname' => $customer['firstname'],
            'lastname' => $customer['lastname'],
            'gender' => $customer['gender'],
            'customer_activated' => $customer['customer_activated'],
            'group_id' => $customer['group_id'],
            'customer_company' => $customer['customer_company'],
            'default_billing' => $customer['default_billing'],
            'default_shipping' => $customer['default_shipping'],
            'is_active' => $customer['is_active'],
            'created_at' => $customer['created_at'],
            'updated_at' => $customer['updated_at'],
            'customer_invoice_email' => $customer['customer_invoice_email'],
            'customer_extra_text' => $customer['customer_extra_text'],
            'customer_due_date_period' => $customer['customer_due_date_period'],
        ]);

        if (!is_array($customer['address'])) continue;

        $sql_address = "INSERT INTO `address`(`id`, `customer_id`, `customer_address_id`, `email`, `firstname`, `lastname`, 
`postcode`,`street`, `city`, `telephone`, `country_id`, `address_type`, `company`, `country`)
VALUES(:id, :customer_id, :customer_address_id, :email, :firstname, :lastname, 
:postcode, :street, :city, :telephone, :country_id, :address_type, :company, :country)";
        $stm_insert_address = $pdo->prepare($sql_address);
        $stm_insert_address->execute([
            'id' => $customer['address']['id'],
            'customer_id' => $customer['address']['customer_id'],
            'customer_address_id' => $customer['address']['customer_address_id'],
            'email' => $customer['address']['email'],
            'firstname' => $customer['address']['firstname'],
            'lastname' => $customer['address']['lastname'],
            'postcode' => $customer['address']['postcode'],
            'street' => $customer['address']['street'],
            'city' => $customer['address']['city'],
            'telephone' => $customer['address']['telephone'],
            'country_id' => $customer['address']['country_id'],
            'address_type' => $customer['address']['address_type'],
            'company' => $customer['address']['company'],
            'country' => $customer['address']['country'],

        ]);
        echo "<p>Nu är alla tillagda.</p>";
    }
};