<?php
/**
 * Created by PhpStorm.
 * User: annagustafsson
 * Date: 2017-11-07
 * Time: 14:52
 */
$wordlist = [
    'system' => 0,
    'handel' => 0,
    'med' => 0,
];
$words = [
    'Anna' => 0,
    'Kaisa' => 0,
    'medie' => 0,
];


function get_webpage($url)
{
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);

    $response = curl_exec($ch);
    curl_close($ch);

    return $response;
}

function count_words($haystack, array $needles)
{
    $haystack = strip_tags($haystack);
    $haystack = strtolower($haystack);

    $result = [];
    foreach ($needles as $word => $value) {
        $nr = substr_count($haystack, $word, 0);
        $result[] = $word . " - " . $nr;
    };
    return $result;
}
$page = get_webpage("http://medieinstitutet.se/webbutvecklare-ehandel/");

?>
    <ul>
        <?php foreach (count_words($page, $words) as $wordCount): ?>
            <li><?= $wordCount ?></li>
        <?php endforeach; ?>
    </ul>

