<?php
/**
 * Created by PhpStorm.
 * User: annagustafsson
 * Date: 2017-11-09
 * Time: 10:28
 */
$host = 'localhost';
$db = 'wieg16';
$user = 'root';
$password = 'root';
$charset = 'utf8';
$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$options = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES => false];

$pdo = new PDO($dsn, $user, $password, $options);

if (isset($_POST['submit'])) {
    $sql = "INSERT INTO `form`(`name`, `email`)VALUES(:name, :email)";
    $stm_insert = $pdo->prepare($sql);
    $stm_insert->execute(['name' => $_POST['fname'], 'email' => $_POST['femail']]);
    echo "<p>Lade till en ny användare!</p>";
}